from picamera import PiCamera
from time import sleep
import http.server
import socketserver
import threading
import os

statinfo = None
img_size = None
def tomarFoto():
	global img_size
	global statinfo
	camera = PiCamera()
	camera.start_preview()
	sleep(5)
	camera.capture('/home/pi/image.jpg')
	camera.stop_preview()
	img = open('/home/pi/image.jpg', 'rb')
	statinfo = os.stat('/home/pi/image.jpg')
	img_size = statinfo.st_size

class MyHandler(http.server.SimpleHTTPRequestHandler):
	def do_HEAD(self):
		self.send_response(200)
		self.send_header("Content-type", "image/jpg")
		self.send_header("Content-length", img_size)
		self.end_headers()
			
	def do_GET(self):
		tomarFoto()
		self.send_response(200)
		self.send_header("Content-type", "image/jpg")
		self.send_header("Content-length", img_size)
		self.end_headers()
		f = open('/home/pi/image.jpg', 'rb')
		self.wfile.write(f.read())
		f.close()         
			
class MyServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
	def __init__(self, server_adress, RequestHandlerClass):
		self.allow_reuse_address = True
		socketserver.TCPServer.__init__(self, server_adress, RequestHandlerClass, False)
					
if __name__ == "__main__":
	HOST, PORT = "localhost", 3000
	server = MyServer((HOST, PORT), MyHandler)
	server.server_bind()
	server.server_activate()
	server_thread = threading.Thread(target=server.serve_forever)
	server_thread.start()